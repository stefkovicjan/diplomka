1
Dobrý deň, chcel by som vám predstaviť svoju prácu s názvom Inovatívny prístup k realizácii výpočtov na superpočítačových infraštruktúrach.

2 Motivation
Superpočítačové infraštruktúry alebo HPC prepájajú výpočetné a úložné zdroje za účelom spracovania náročných úloh, ktoré by na jednom uzle trvali príliš dlho, alebo potrebujú rozsiahlejší úložný priestor. Jedná sa napríklad o vedecké výpočty, modelovanie, rendrovanie a simulácie.
Schéma znázorňuje zjednodušený pohľad na českú National Grid Infrastructure. Užívateľ sa zo svojho počítača pripojí na frontend, kde zadáva úlohy s požiadavkami na zdroje. Plánovač ich následne berie z fronty a umiestňuje na výpočetné a úložné uzly.
Cieľom tejto práce je preskúmať možnosti, ako tento proces užívateľsky spríjemniť a priblížiť bežnej práci s desktopom. Napríklad aby užívateľ nemusel odhadovať koľko zdrojov bude úloha potrebovať, alebo kde sa nachádza v infraštruktúre nachádza, či sa presúva, a podobne. Okrem toho treba vziať do úvahy všetky tradičné typy úloh.

3 Current practices
K typom úloh z pohľadu užívateľskej interakcie, s ktorými sa tradične stretávame, patria:
	-dávkové úlohy, pri ktorých iba zadáme výpočet a ďalej aktívne nezasahujeme
	-interaktívne, či už textové alebo grafické - GUI sa väčšinou tuneluje alebo využívame remote desktop
	-cloud computing - širší pojem pre situácie, keď dostaneme celý virtuálny stroj, zväčša aj s administrátorským prístupom

4 Design proposal
Tieto tradičné úlohy by sme chceli realizovať na HPC tak, aby sa práca užívateľa najviac podobala jeho práci s obyčajným desktopom.
Komponenty, s ktorými užívateľ bežne pracuje, teda pracovná plocha a "pod ňou" skryté zdroje, by sa dala nasledovne preložiť do HPC prostredia.
Práca s textom aj grafikou by sa diala na virtuálnej ploche, a alokovanie zdrojov by bolo od užívateľa skryté a automatizované.
Tu neskôr uvidíme náhradu tradičného žiadania o zdroje za dynamickú prácu s kontajnermi.

5 Containers
Na dosiahnutie tohto zlepšenia som skúmal súčasný trend kontajnerov. Od kontajnerov očakavám na jednej strane zlepšenie pre užívateľa a na strane druhej technické výhody ako také. Ústrednou myšlienkou je "čo úloha - to kontajner". Úlohy budeme spúšťať v kontajneroch a tieto môžu podľa potreby vznikať a zanikať, presúvať sa medzi servermi, komunikovať a spolupracovať.

6 User requirements
Na tomto slajde už mám požiadavky ktoré som v práci spolu s technologickou vhodnosťou sledoval:
	-použitie infraštruktúry sa veľmi nelíši od mojej bežnej práce s osobným počítačom
	-rozhodnutia robím len keď je to nutné - na osobnom počítači napríklad nerozdeľujem zdroje, nevyberám server na ktorom sa aplikácia spustí - mám totiž len ten jeden svoj, zdroje majú iba možnosti "je ich dosť" / "nie je ich dosť", licencie na programy alebo GPU tam buď mám alebo nemám
	-vidím iba informácie, ktoré práve potrebujem, napríklad záťaž iba ak sa odo mňa vyžaduje nejaká akcia
	-o správu zdrojov sa starám čo najmenej
	-môžem si aplikácie a prostredie customizovať, tieto nastavenia si uložiť, prípadne zdieľať s kolegami

7 Discussion on selected containerization tools
Práca sa najprv venuje rozsiahlemu výskumu súčasneho stavu integrácie kontajerov s HPC. Konkrétnejšie som si potom zvolil tieto tri nástroje.
Mojím cieľom bolo posúdiť ich vhodnosť na naše požiadavky, konkrétne typy úloh a užívateľské interakcie s ich pomocou rozobrať podrobnejšie a pokiaľ možno aj vyskúšať na testbede.
Prvý je Docker, ktorý ma oslovil hlavne popularitou a s tým súvisiacou podporou od iných nástrojov a komunity, a prísľubom ďalšieho vývoja v budúcnosti. Má veľmi dobre vyriešenú customizáciu obrazov, správu zdrojov a orchestráciu viacerých kontajnerov, čo sa nám hodí neskôr pri diskusii taskov ako je pole úloh a paralelné výpočty.
OpenVZ je technologicky založený inak, spomenuté veci zvláda tiež, hoci niekedy slabšie, ale mal by podporovať checkpointing a živú migráciu, ktorá sa v HPC zíde (Docker ju momentálne nepodporuje).
Podman sám seba prezentuje ako alternatívu k Dockeru s trochu inými princípmi a extra funkcionalitou, a je integrovaný s Red Hat Enterprise Linux.

8 Testbed
Na svoje experimenty som mal zostavený testbed na ktorý som nainštaloval rôzne host OS, kontajnerizačné technológie, skúmal široké možnosti sieťovania a manažovania, vzdialené pripojenie a prenos grafického prostredia. Navrhol som adresovanie, zvolil si aplikácie na testovanie a pripravil vlastné konfigurácie a jeden Docker image.
Ako testový scenár som väčšinou volil výpočty v R software, pričom som využil dva existujúce a jeden vlastný Docker image.

9 Batch and interactive jobs
Batch úlohy a interaktivita sú základné zadania, ktoré musí naša infraštruktúra zvládnuť. Kontajner môže akceptovať dávkovú úlohu a poslať naspäť výstup. Ako vstupný bod interaktívneho kontajnera môžeme definovať aplikáciu, ktorá v ňom beží, užívateľ teda o kontajneri ani nevie. Alebo môže byť vstupný bod príkazový riadok a teda užívateľ môžu spravovať aj veci mimo aplikáciu.
Technicky sa potom dá do kontajnera vstúpiť cez manager, na zverejnený port alebo priamo SSH na IP adresu. Niektoré kontajnery zase spúšťajú službu, ku ktorej sa dá pristúpiť cez prehliadač.
V práci som skúmal všetky tieto možnosti a snažil sa každý pokus priblížiť čo najviac užívateľskej skúsenosti s desktopom. Ideálne teda stačilo kliknúť na ikonu, ktorá spustila skript a ten sa postaral o vytvorenie a zrušenie kontajnera, a jeho poskytnutie užívateľovi akoby to bola bežná aplikácia bežiaca na jeho ploche.

Dva najčastejšie spôsoby tunelovanie GUI sú SSH -X a VNC. Mne sa väčšinu cieľov podarilo realizovať pomocou SSH -X, čo je vlastne prenos X window cez SSH. V jednom experimente som tiež pracoval s mapovaním X11 socketu medzi hostom a kontajnerom, vďaka čomu by kontajner nepotreboval poskytovať SSH server - čo je pri niektorých kontajneroch cieľ či už kvôli bezpečnosti alebo na odľahčenie.

10 Resource management
Ďalej som skúmal funkcionalitu, ktorá pre užívateľa už nie je priamo viditeľná, ale je dôležitá pre HPC prostredie.
Prvý problém je správa zdrojov kontajnerov. Tu som sa stretol s dobrou podporou v niektorých zvolených tooloch, kontajnerom je možné obmedziť limity na zdroje a tieto sa dajú upravovať aj za behu, pričom kontajner reaguje ako treba. To má využitie v našom designe, kde užívateľ nemusí premýšľať nad zdrojmi ale kontajner je naalokovaný s určitým defaultnym minimom, ktoré sa následne dá zdvihnúť, ak to kapacita hosta umožňuje. Všetko sa dá realizovať pomocou automatizovaných volaní, nastaviť skript čo bude využitie sledovať, a podobne.

11 Checkpointing and live migration
To ma priamo privádza k ďalšej funkcionalite - čo ak nám úloha beží, ale zdroje nestačia. V tom prípade sa nám hodí premigrovať kontajner na host s vyššou kapacitou. Ideálny prípad: teda kde výpočet nie je prerušený a nedôjde k chybe, a užívateľ si všimne iba malé "zamrznutie" ak pracuje interaktívne, je živá migrácia.
Práve tu sa najviac prejavili rozdielnosti v tooloch - hoci Docker prevažoval v ostatných oblastiach, nepodporuje v súčasnosti checkpointing.
Na Podmane sa mi podarilo zrealizovať checkpointing so zachovaním stavu aj po reboote hosta, ale už nie s presunom na iný host.
Na OpenVZ som zrealizoval úplnú živú migráciu vrátane neprerušenej práce so vzdialeným pripojením a grafickým prostredím.

12 Arrays of jobs, parallel and distributed jobs
Jednoduché polia úloh sa dajú prirodzene premietnuť z tradičného prostredia do toho založeného na kontajneroch. Navyše môžme využiť orchestračné nástroje, buď tie natívne kontajnerom, alebo tretích strán.
Paralelizované výpočty vnútri jednej inštancie sú tiež bežné a väčšinou vo forme dedikovaných kontajnerov.
Zato distribuované výpočty na viacerých uzloch a ich komunikácia cez MPI (čo je teraz najpoužívanejší štandard pre distribuovanosť v HPC) je stále predmetom výskumu a integrácie MPI a správy kontajerov sa venujú nedávne články, ktoré som v práci rozobral.

13 Results
Táto práca prináša tri hlavné výsledky.
Prvým je rozsiahly prieskum aplikácie kontajnerizácie na HPC infraštruktúry, hlavne z pohľadu užívateľskej interakcie. Práca čitateľovi vysvetľuje všetky dôležité oblasti a prezentuje aktuálny stav výskumu a vývoja.
Druhým je dôkladná analýza zvolených kontajnerizačných a ďalších podporných nástrojov a posúdenie ich vhodnosti na rôzne typy úloh.
Tretím je praktický dizajn riešenia, ktoré tieto nástroje dáva dohromady, prekonanie problémov ako je sieťovanie v infraštruktúre alebo správa hostov a kontajnerov, a demonštrácia niektorých príkladov. Práca sa tiež snaží na testbede vyriešiť alebo aspoň uľahčiť prácu budúcemu návrhu reálnej infraštruktúry a poskytuje námety na ďalší výskum, ako posunúť užívateľskú skúsenosť.


Ďakujem za pozornosť.

----------------------
Pripomienky (iba ak sa opýtajú):
1.) O nástroji Open OnDemand som nevedel. Pravdepodobne pretože systém nie je založený na kontajnerizácii. Takisto som v práci neskúmal všetky riešenia používané v CERIT-SC, zameriaval som sa na úlohy alebo problémy.

2.) Táto pripomienka sa vzťahuje k vete ktorú som uviedol iba ako jednu z možností začiatku výpočtu. Súhlasím, že sa radí k tým menej praktickým.

3.) Tu musím opäť súhlasiť a vo finálnom dizajne by som tento údaj zrejme zaradil pod rozšírené informácie, ktoré by defaultne neboli zobrazené.

4.) Meno vedúceho spomínam niekoľkokrát uprostred práce predovšetkým "pre istotu" aby som si omylom neprivlastnil cudzie zásluhy. Inak práca prebiehala ako každá iná - s prvotným nápadom prišiel vedúci, ja som ho následne spracovával do hĺbky a pridával nápady na ktoré som narazil po ceste.

5.) Pri diskusiu ohľadne grafických nástrojov som postupoval rovnako ako pri kontajnerizačných a podporných nástrojoch: hoci dnes existujú komplexné riešenia, komerčné aj open-source, chcel som v práci budovať myšlienku od základu. Predstavené nástroje sú teda dobre ilustrujú tento postup, aj keď finálny design by uvažoval hlavne ich modernejších nástupcov.

6.) Ukončenie aplikácie pri rýchlom náraste nárokov nastať môže. Budú však existovať aj situácie, pri ktorých nároky neporastú tak rýchlo, a reagovanie prinesie úžitok. Pri ukončených aplikáciách sa navyše stav môže zopakovať a pri novom spustení urobiť lepší odhad. Nie je to teda riešenie na každý prípad, ale je to dostatočne veľké zlepšenie pre správu zdrojov a pre užívateľskú prácu, že má zmysel ho skúmať a pokúšať sa oň.

Questions
1.) Práca pôvodne začala byť realizovaná v MetaCentrum Cloude. Kvôli obmedzeniam na mojom užívateľskom účte a technickým problémom, plus obave vedúceho práce, že zanorená virtualizácia môže spôsobovať niektoré z nich, mi bolo umožnené pracovať na vlastnom hardvéri. Mnoho technických problémov sa nakoniec ukázalo nezávislých od tohoto faktu, nedá sa však vylúčiť, že by nenastali ďalšie.

2.) Svoje riešenie som chcel budovať od low-levelu. K využitiu browseru som nakoniec taktiež dospel, pretože ako uvádzam v práci, mnohé aplikačné kontajnery sú vytvorené tak, aby bola aplikácia ovládaná práve cez browser a neobsahoval SSH server. Vo svojom výskume som toto pochopil ako jednu z možností vzdialeného pripojenia.

3.) Klienstká časť infraštruktúry sa dá určite realizovať aj na Windowse. Prevod návrhu frontu z Linuxového typu na Windows by sa v tejto práci nelíšil od iných projektov.

4.) Táto otázka je príklad ďalšieho komplexného scenára, ktorých by sa vždy dal k práci pridať mnoho. Mobilita klienta by si teda vyžiadala ďalšiu kapitolu. V práci je v rámci prieskumu spomenutý nástroj, ktorý implementuje medzivrstvu na udržanie spojenia. Pri riešení mobility klienta by som teda začal svoj výskum tu, a určite sa pokúsil využiť riešenia tohoto problému ako ich poznáme napríklad z telefónnej siete.

5.) Pri výskume som išiel smerom zisťovania "čo všetko sa dá zrealizovať". Pri reálnom projekte by som už bezpečnosť musel zahrnúť od začiatku výstavby infraštruktúry. Pri každej podtéme som ale na otázky bezpečnosti narazil, či už ide o užívateľské práva, rootful/rootles kontajnery, alebo obrazy so zahrnutými opatreniami. 

----------------------
-asi 12 slidov
-slajd na minútu
-počítať s 12 minútami na prezentáciu

-viac sa sústrediť na hlavný task, netreba obecne hovoriť o technológiách
-musí z toho byť vidieť cieľ a to čo som urobil/nadesignoval
-testoval som toto, narazil na neúspech tu
-treba z toho vidieť tú moju prácu, predávam (aj agresívne) svoju prácu
-komisia musí chápať aký problém sa rieši
-komisia sa musí dozvedieť, čo tá diplomka urobila

----------------------

