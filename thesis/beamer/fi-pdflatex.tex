%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% I, the copyright holder of this work, release this work into the
%% public domain. This applies worldwide. In some countries this may
%% not be legally possible; if so: I grant anyone the right to use
%% this work for any purpose, without any conditions, unless such
%% conditions are required by law.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[xcolor=table]{beamer}
\usetheme{CambridgeUS}
\usecolortheme{seahorse}
\usepackage[utf8]{inputenc}
\usepackage[
  main=english, %% By using `czech` or `slovak` as the main locale
                %% instead of `english`, you can typeset the
                %% presentation in either Czech or Slovak,
                %% respectively.
  czech, slovak %% The additional keys allow foreign texts to be
]{babel}        %% typeset as follows:
%%
%%   \begin{otherlanguage}{czech}   ... \end{otherlanguage}
%%   \begin{otherlanguage}{slovak}  ... \end{otherlanguage}
%%
%% These macros specify information about the presentation
\title{Master's Thesis} %% that will be typeset on the
\subtitle{Innovative Computing Approach for HPC Infrastructures} %% title page.
\author{Bc. Ján Štefkovič}
\date{February 2021}
%% These additional packages are used within the document:
\usepackage{ragged2e}  % `\justifying` text
\usepackage{booktabs}  % Tables
\usepackage{tabularx}
\usepackage{tikz}      % Diagrams
\usetikzlibrary{calc, shapes, backgrounds}
\usepackage{amsmath, amssymb}
\usepackage{url}       % `\url`s
\usepackage{listings}  % Code listings
\usepackage{xcolor}
\usepackage{graphicx}
\definecolor{tableColor1}{HTML}{FFECB3}
\definecolor{tableColor2}{HTML}{FFF9E5}

\frenchspacing
\begin{document}
  \frame{\maketitle}

%  \AtBeginSection[]{% Print an outline at the beginning of sections
%    \begin{frame}<beamer>
%      \frametitle{Outline for Section \thesection}
%      \tableofcontents[currentsection]
%    \end{frame}}

\section{Thesis}
\subsection{Introduction}

\begin{frame}{Motivation}
\textbf{High-performance Computing Infrastructure} aggregates computing and storage resources together with network and software in a way that supports performance-intensive tasks.
\begin{figure}[h]
 	\centering
	\includegraphics[scale=.75]{HPC.pdf}
\end{figure}
\textbf{Goal:} improve user experience of interaction with~HPC Infrastructures
\begin{itemize}
\item can we achieve desktop-like feeling?
\item can we abstract from resource allocation requests?
\item can we still support all types of traditional HPC tasks?
\end{itemize}
\end{frame}

\begin{frame}{Current practices}
Traditional types of user interaction:
\begin{itemize}
\item \textbf{batch jobs} – perform computations described in script files
\item \textbf{interactive jobs} – provide interactive textual mode, or graphical environment
\item \textbf{cloud computing} – provide a virtual machine with chosen application equipment
\end{itemize}
\end{frame}

\begin{frame}{Design proposal}
\begin{figure}[h]
 	\centering
	\includegraphics[keepaspectratio=true, width=\textwidth]{comparison.pdf}
	\caption{Components of new design compared to a personal computer}\label{comparison}
\end{figure}
\end{frame}

\begin{frame}{Containers}
\begin{columns}[T]
\begin{column}{.5\textwidth}
\begin{figure}[h]
	\centering
	\includegraphics[keepaspectratio=true, width=55mm]{containers.pdf}
	\caption{Containers}\label{containerization}
\end{figure}
\end{column}
\begin{column}{.5\textwidth}
What we expect:
\begin{itemize}
\item fast creation / deletion
\item customization of environment
\item reproducibility
\item manageability
\item resource management
\item scaling
\item orchestration
\item isolation
\item portability
\item live migration
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{User requirements}
\begin{itemize}
\item the use of HPC infrastructure does not differ much from the use of a personal computer
\item the user is asked to make decisions only when necessary
\item the user is well informed, but not distracted
\item the resources can be managed automatically
\item the applications allow custom user modifications
\end{itemize}
\end{frame}

\subsection{The analysis of design proposal against requirements}

\begin{frame}{Discussion on selected containerization tools}
\begin{columns}[T]
\begin{column}{.5\textwidth}
\begin{itemize}
\item Docker
	\begin{itemize}
	\item community support
	\item custom configuration files (Dockerfile) and built images
	\item resource monitoring and management
	\item orchestration support
	\end{itemize}
\item OpenVZ
	\begin{itemize}
	\item many of the previous
	\item open-source version has less options
	\item live-migration
	\end{itemize}
\item Podman
	\begin{itemize}
	\item Docker alternative
	\item extra functionality
	\item checkpointing
	\end{itemize}
\end{itemize}
\end{column}
\begin{column}{.5\textwidth}
\begin{figure}[h]
	\centering
	\includegraphics[keepaspectratio=true, width=25mm]{docker-logo.png}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[keepaspectratio=true, width=25mm]{openvz-logo.png}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[keepaspectratio=true, width=50mm]{podman-logo.png}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\subsection{Testbed}

\begin{frame}{Testbed}
\begin{columns}[T]
\begin{column}{.5\textwidth}
\begin{figure}[h]
 	\centering
	\includegraphics[keepaspectratio=true, width=55mm]{status.png}
\end{figure}
\end{column}
\begin{column}{.5\textwidth}
\begin{figure}[h]
 	\centering
	\includegraphics[keepaspectratio=true, width=55mm]{testbed.pdf}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\subsection{Traditional scenarios realized with containers}

\begin{frame}{Batch and interactive jobs}
\begin{columns}[T]
\begin{column}{.5\textwidth}
\begin{itemize}
\item run a batch job
\item enter the container from a manager on host
\item expose a port
\item SSH into the container
\item use single purpose container images
\item channel GUI via ssh -X
\end{itemize}
\end{column}
\begin{column}{.5\textwidth}
\begin{figure}[h]
 	\centering
	\includegraphics[keepaspectratio=true, width=60mm]{7-3-gui.png}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Resource management}
\begin{columns}[T]
\begin{column}{.5\textwidth}
\begin{itemize}
\item not ``visibly'' connected to user experience
\item resource management tested and proven well-supported
\item essential for HPC environment
\end{itemize}
\end{column}
\begin{column}{.5\textwidth}
\begin{figure}[h]
	\centering
	\includegraphics[keepaspectratio=true, width=45mm]{glxgears.png}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[keepaspectratio=true, width=45mm]{resources-graphs.pdf}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Checkpointing and live migration}
\begin{columns}[T]
\begin{column}{.5\textwidth}
\begin{itemize}
\item more advanced resource utilization
\item considering entire HPC infrastructure
\item Docker offline migration
\item Podman checkpoint and restore
\item OpenVZ live migration
\end{itemize}
\end{column}
\begin{column}{.5\textwidth}
\begin{figure}[h]
	\centering
	\includegraphics[keepaspectratio=true, width=60mm]{migration.png}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Arrays of jobs, parallel and distributed jobs}
\begin{itemize}
\item classic approach with imperative program
\item orchestration and declarative view
\item parallelism in dedicated instances
\item integration of MPI with containers is a subject of current research
\end{itemize}
\end{frame}


\subsection{Conclusion}
\begin{frame}{Results}
\begin{itemize}
\item extensive \textbf{research} of application of containers on HPC
\item \textbf{analysis} of chosen tools with regards to various types of tasks
\item \textbf{realization} of an example design, accompanied with demonstrations and solutions to particular problems
\end{itemize}
\end{frame}

%\section{Questions}
%\begin{frame}{Questions}
%\framesubtitle{Question 1}
%\begin{itemize}
%\item Answer 1
%\end{itemize}
%\end{frame}
%
%\section{Reactions to reviews}
%\begin{frame}{Reaction to area 1}
%\begin{itemize}
%\item Explanation of area 1
%\end{itemize}
%\end{frame}
%
%\section{Conclusionof defence}
%\begin{frame}{Conclusion of defence}
%\begin{itemize}
%\item Defence to problematic area 1
%	\begin{itemize}
%	\item explanation 1
%	\end{itemize}
%\item Defence to problematic area 2
%	\begin{itemize}
%	\item explanation 2
%	\end{itemize}
%\end{itemize}
%\end{frame}

\end{document}
