#! /bin/bash

# Use: ./7_1-batch-job-docker.sh <script>.R <remote_server_address>

echo Demo 7_1-batch-job-docker. Take a batch job in the script and execute it in a container on the server.
echo Executing script $1 on the server $2

# Copy the script file to the server.
scp $1 $USER@$2:

# Execute the job in a docker container on the remote server. PWD is remote, $1 is local.
ssh $2 'docker run --name demo-7_1-batch-job-docker --rm -v "$PWD":/home/docker -w /home/docker -u docker r-base R CMD BATCH "'$1'"'

# Move the results back.
scp $USER@$2:$1out .

# Delete the files from the remote server.
ssh $2 'rm "'$1'" "'$1'"out'

echo View your result in $1out

