#!/bin/bash

counter=0

until [ $counter -gt 5000 ]
do
  name=$(uname -n)
  echo "$name: $counter" >> counter_output.txt
  ((counter++))
  sleep 1
done
