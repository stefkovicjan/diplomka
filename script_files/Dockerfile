FROM ubuntu:16.04

# install dependencies
RUN apt-get update && \
    apt-get install -y \
    wget \
    libclang-dev \
    libxkbcommon-x11-0 \
    libxslt-dev \
    gstreamer1.0-plugins-base \
    iso-codes \
    libcdparanoia0 \
    libgstreamer-plugins-base1.0-0 \
    libgstreamer1.0-0 \
    libjpeg62 \
    libogg0 \
    libopus0 \
    liborc-0.4-0 \
    libtheora0 \
    libvisual-0.4-0 \
    libvorbis0a \
    libvorbisenc2 \
    nano

# install xauth
RUN apt-get update && \
    apt-get install -y \
    xauth

# install R
RUN apt-get update && \
    apt-get install -y \
    r-base

# install Rstudio
RUN wget https://download1.rstudio.org/rstudio-xenial-1.1.414-amd64.deb && \
    dpkg -i rstudio-xenial-1.1.414-amd64.deb

# install SSH and configure root with password
# https://docs.docker.com/engine/examples/running_ssh_service/

RUN apt-get update && \
    apt-get install -y \
    openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:secret' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# following two options should be enabled, in our image only second is missing
# X11Forwarding yes
# X11UseLocalhost no

RUN echo "X11UseLocalhost no" >> /etc/ssh/sshd_config

EXPOSE 22

# start the SSH service
CMD ["/usr/sbin/sshd", "-D"]

# to build run: docker build -t r-studio-on-u-16-04 .
