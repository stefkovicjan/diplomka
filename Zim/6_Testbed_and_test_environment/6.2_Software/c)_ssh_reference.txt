Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-03-09T11:39:41+01:00

====== c) ssh reference ======

https://linux.die.net/man/1/ssh
useful options:

**-a'** Disables forwarding of the authentication agent connection. 

**-F** //configfile//
Specifies an alternative per-user configuration file. If a configuration file is given on the command line, the system-wide configuration file (///etc/ssh/ssh_config//) will be ignored. The default for the per-user configuration file is //~/.ssh/config//.

**-f'** Requests **ssh** to go to background just before command execution. This is useful if **ssh** is going to ask for passwords or passphrases, but the user wants it in the background. This implies **-n**. The recommended way to start X11 programs at a remote site is with something like **ssh -f host xterm**. 
If the **ExitOnForwardFailure** configuration option is set to ''"yes"'', then a client started with **-f** will wait for all remote port forwards to be successfully established before placing itself in the background. 

**-i** //identity_file//
Selects a file from which the identity (private key) for RSA or DSA authentication is read. The default is //~/.ssh/identity// for protocol version 1, and //~/.ssh/id_rsa// and //~/.ssh/id_dsa// for protocol version 2. Identity files may also be specified on a per-host basis in the configuration file. It is possible to have multiple **-i** options (and multiple identities specified in configuration files). 

**-n'** Redirects stdin from ///dev/null// (actually, prevents reading from stdin). This must be used when **ssh** is run in the background. A common trick is to use this to run X11 programs on a remote machine. For example, **ssh -n shadows.cs.hut.fi emacs &** will start an emacs on shadows.cs.hut.fi, and the X11 connection will be automatically forwarded over an encrypted channel. The **ssh** program will be put in the background. (This does not work if **ssh** needs to ask for a password or passphrase; see also the **-f** option.)

**-p** //port//
Port to connect to on the remote host. This can be specified on a per-host basis in the configuration file. 

**-t'** Force pseudo-tty allocation. This can be used to execute arbitrary screen-based programs on a remote machine, which can be very useful, e.g. when implementing menu services. Multiple **-t** options force tty allocation, even if ssh has no local tty. 

**-X'** Enables X11 forwarding. This can also be specified on a per-host basis in a configuration file.
X11 forwarding should be enabled with caution. Users with the ability to bypass file permissions on the remote host (for the user's X authorization database) can access the local X11 display through the forwarded connection. An attacker may then be able to perform activities such as keystroke monitoring.
For this reason, X11 forwarding is subjected to X11 SECURITY extension restrictions by default. Please refer to the **ssh -Y** option and the **ForwardX11Trusted** directive in ssh_config(5) for more information. 

**-Y'** Enables trusted X11 forwarding. Trusted X11 forwardings are not subjected to the X11 SECURITY extension controls. 

**ssh** may additionally obtain configuration data from a per-user configuration file and a system-wide configuration file. The file format and configuration options are described in ssh_config(5).

**Authentication** - see manpages

**X11 FORWARDING**
If the **ForwardX11** variable is set to ''"yes"'' (or see the description of the **-X**, **-x**, and **-Y** options above) and the user is using X11 (the DISPLAY environment variable is set), the connection to the X11 display is automatically forwarded to the remote side in such a way that any X11 programs started from the shell (or command) will go through the encrypted channel, and the connection to the real X server will be made from the local machine. The user should not manually set DISPLAY. Forwarding of X11 connections can be configured on the command line or in configuration files.
The DISPLAY value set by **ssh** will point to the server machine, but with a display number greater than zero. This is normal, and happens because **ssh** creates a ''"proxy"'' X server on the server machine for forwarding the connections over the encrypted channel.
**ssh** will also automatically set up Xauthority data on the server machine. For this purpose, it will generate a random authorization cookie, store it in Xauthority on the server, and verify that any forwarded connections carry this cookie and replace it by the real cookie when the connection is opened. The real authentication cookie is never sent to the server machine (and no cookies are sent in the plain).

If the **ForwardAgent** variable is set to ''"yes"'' (or see the description of the **-A** and **-a** options above) and the user is using an authentication agent, the connection to the agent is automatically forwarded to the remote side. 

**Environment**
**ssh** will normally set the following environment variables: 
DISPLAY' The DISPLAY variable indicates the location of the X11 server.  It is automatically set by **ssh** to point to a value of the form "''hostname:n"'', where "''hostname"'' indicates the host where the shell runs, and 'n' is an integer â‰¥1. **ssh** uses this special value to forward X11connections over the secure channel.  The user should normally not set DISPLAY explicitly, as that will render the X11 connection insecure (and will require the user to manually copy any required authorization cookies).
...

**Files** - see manpages

https://linux.die.net/man/5/ssh_config
