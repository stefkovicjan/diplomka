Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-07-14T13:44:56+02:00

====== a) Log ======

14.7.
-registration into MetaCentrum
-registration into Cloud

21.7.
-read through Perun web gui: https://perun.cesnet.cz/fed/gui/
-read through MetaCentrum VO: https://metavo.metacentrum.cz/

25.-27.7.
-read through MetaSeminar-HandsOn.pdf and a certain number of wiki pages

28.7.
-Pokus Quick start

4.8.
-Pokus interactive

10.8.
-Pokus batch

23.9.
-by this point: -read most of https://wiki.metacentrum.cz/wiki/How_to_compute and https://wiki.metacentrum.cz/wiki/Kategorie:Pro_u%C5%BEivatele
			-tried most of "MetaSeminar-HandsOn.pdf"

25.9.
-has two instances, can connect to one of them, tries to figure out how to connect to the second one; studied and run docker on the first instance

30.9.
-studied live migration on Docker and OpenVZ, prepared counter.sh

3.10.
-vytvorit si 2x2 cloudove stroje (2x OpenVZ a 2x Docker) [malo by stacit 1 CPU, 2 GB memory, 80 GB disk == standard.small]

4.10.
-Docker image migration

6.10.
-studied and tried Docker live migration by Checkpoint and restore with CRIU; unsuccessful - not supported anymore

6.11.
-created 2 Ubuntu instances for OpenVZ and interconnected them with paswordless ssh; did not work with OpenVZ

9.11.
-created 2 Debian 9 instances for OpenVZ and interconnected them with paswordless ssh; successfully installed OpenVZ and set it as only kernel https://wiki.openvz.org/Installation_on_Debian_9
-managed to do offline migration
-online migration throws segmentation fault (on suspending of running container)

11.2.
-during last month tried to run OpenVZ containers inside Virtualbox. Required more configuration then the cloud. Unable to start a container

17.2.
-stopping live migration experiments. Created new plan for the whole thesis. Wrote a User Story. Got access to two physical servers.

21.2.
-created machines front, server-1 and server-2 on vmware server. All have Debian 9 and all are on VM Network (for now)

22.2.
-connected machines with passwordless ssh
-connected with ssh -X

24.2.
-started VNC server session on server-1, connected to it from front

25.2.
-reinstalling 21.2.-24.2., because at first I used 32-bit instead of 64-bit by mistake; installed docker to server-1

2.3.
-an idea with VNC: setting //~/.vnc/xstartup //so that it creates a container and enters it. This really worked with connecting front to container. (scenario c))

8.3.
-scenario d): successfully started GUI app in container and displayed it locally on server-1, then also remotely to front

10.3.
-scenario e): migrate non-GUI case with (re-)connecting front
-scenario f): migrate GUI case

12.3.
-consultation, finalized goal

14.3.
-installed 2 openvz machines, connected with passwordless ssh; tried migration and unsuccessfully live migration

16.3.
-prepared LaTeX for thesis and template file with examples

17.3.
-wrote new detailed User Story and outline

18.3.
-rewriting personal notes according to outline. Now working on both practical and theoretical part at once

19.3.
-prepared some HPC theory and went over //MetaSeminar-HandsOn.pdf// with taking notes

28.3.
-this week research articles, wrote Introduction and Chapter 2, including references, pictures, formatting and grammar check (all in TeX)

31.3.
-Chapter 3 done, including references, pictures, formatting and grammar check (all in TeX)

7.4.
-Chapter 4  done, including references, pictures, formatting and grammar check (all in TeX)

9.4.
-found Docker container with R, Docker container with Rstudio in a browser and prepared Docker container with Rstudio GUI

12.4.
-now I can run remote CLI container by clicking on an icon (on file). I wrote a raw draft of first part of Chapter 5, where I explain how to prepare testbed

13.4.
-first part of Chapter 5 written

16.4.
-preparing report, research on migration

18.4.
-two ubuntu 18.04 with Podman, looks promising on migration (was not)

19.4.
-managed to repeat r-base scenario from docker; rstudio scenario failed on connecting to display

24.4.
-included comments from both PDFs

26.4.
-big rework of structure, including comments from mails and videocall; reworked chapters 1, 2, 3

29.4.
-reworked all theory chapters

30.4.
-have two openvz machines and can do live-migration. Problem was building from wrong iso

27.5.
-forgot to write Log for longer time. Took two weeks off to prepare for job interviews. Now trying to run demonstrations and describe them in the last chapter.

28.5.
-I managed to give internet access to openvz containers and install applications in them

29.5.
-reorganized my notes

30.5.
-reworked up to 6.2

31.5.
-up to 6.3 without automatization

14.6.
-it last weeks worked on OpenVZ over ssh -X, live migration, vnc (unsuccessfully)
-had a call which redefined the problem more in detail, need to reorganize / rewrite

20.6.
-we can ssh to Docker containers using exposed port

22.6.
-first draft of many practical sections done, currently everything except three big tasks: migration, resources, parallel

23.6.
-worked on Live migration, found and documented the issue, did not solve

8.7.
-short version of Changing resources

13.7.
-completely revised and corrected up to 6

14.7.
-written Conclusion

19.7.
-written everything and submitted final version. Non-functioning practical parts replaced with discussion

22.9.
-prepared detailed to-do list for review

5.10.
-successfully did OpenVZ live migration (no changes, it just worked) and made a screen recording
-set up Podman on RHEL 8.1 infrastructure, but not yet able to communicate between two custom podman networks on two hosts

11.10.
-created a new DHCP network that works correctly for hosts

17.11.
-worked on L2, unsuccessfully, but it also might not be possible
-rewrote and extended whole theory

4.1.
-did not log properly
-reworked and added everything else
-submitted the final paper
